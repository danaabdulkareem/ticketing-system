<?php

namespace App\Listeners;

use App\Events\TicketStatusUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SetStatusToActive
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketStatusUpdated $event): void
    {
        $ticket = $event->ticket;
        if ($ticket->total_working_hours && $ticket->technicians_id) {
            $ticket->status = '2';
            $ticket->save();
        }
    }
}
