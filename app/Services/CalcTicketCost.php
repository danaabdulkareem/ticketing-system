<?php
namespace App\Services;

use App\Models\Ticket;

class CalcTicketCost{
    protected $model;
    function __construct()
    {
        $this->model = new Ticket();
    }
    public function checkTicket($id){
        $ticket = Ticket::where('id', $id)->first();
        if(!$ticket){
            return response (["message"=>"no such ticket found"]);
        }
        return $ticket;
    }
    public function cost($ticket){
        $servicePrice = $ticket->service->price;
        $techniciansCount = $ticket->technician()->count();
        $hourlyTechnicianPrice = $ticket->technician()->sum('hours_cost');
        $priorityValue = $ticket->priority->value;
        $cost = $servicePrice + ($techniciansCount * $hourlyTechnicianPrice * $priorityValue);
        return $cost;
    }
    public function discount($ticket,$cost){
        if ($ticket->client_evaluation === '1') {
            $cost *= 0.5;
            return $cost;
        }
        elseif($ticket->client_evaluation === '2'){
            $cost *= 0.25;
            return $cost;
        }
        else
        return $cost;
    }

    public function updatetotalcost($id){
        $ticket = $this->checkTicket($id);
        $cost= $this->cost($ticket);
        if ($ticket->client_evaluation) {
        $discountcost = $this->discount($ticket,$cost);
        $ticket->total_cost= $discountcost;}
        else{
            $ticket->total_cost = $cost;
        }
        $ticket->save();
        return response()->json([
            "message" => "updated"
        ]);
    }
}
