<?php

namespace App\Http\Controllers;

use App\Models\Priority;
use Illuminate\Http\Request;

class PriorityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $priorities = Priority::all();
        return $priorities;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'value' => 'required|integer',
        ]);
        $priority = Priority::create([
            'title' => $request['title'],
            'value' => $request['value'],
        ]);
        $response = $priority->get();
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required|string',
            'value' => 'required|integer',
        ]);
        Priority::where('id', $id)->update([
            'title' => $request->input('title'),
            'value' => $request->input('value'),
        ]);

        $response = Priority::where('id', $id)->get();
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Priority = Priority::findOrFail($id);
        $Priority->delete();
        return response()->json(["message" => "priority deleted"], 200);
    }
}
