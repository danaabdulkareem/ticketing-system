<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Status::create([
            'id' => '1',
            'title' => "pending",

        ]);
         Status::create([
            'id' => '2',
            'title' => "active",

        ]);
        Status::create([
            'id' => '3',
            'title' => "do",
        ]);
    }
}
