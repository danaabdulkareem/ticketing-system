<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->constrained('clients')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('priority_id')->nullable()->constrained('priorities')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('status_id')->nullable()->constrained('statuses')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('service_id')->constrained('services')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('client_evaluation')->nullable()->constrained('evaluations')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('client_notes')->nullable()->constrained('notes')->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('total_working_hours')->nullable();
            $table->string('work_report')->nullable();
            $table->float('total_cost')->nullable();
            $table->date('work_completion_date')->nullable();
            $table->date('creation_date')->useCurrent();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
