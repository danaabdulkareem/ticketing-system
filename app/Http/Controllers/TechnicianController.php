<?php

namespace App\Http\Controllers;

use App\Models\Technician;
use Illuminate\Http\Request;

class TechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $technicians = Technician::paginate(3);
        return $technicians;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'hours_cost' => 'required',
        ]);
        $technician = Technician::create([
            'name' => $request['name'],
            'hours_cost' => $request['hours_cost'],
        ]);
        $response = $technician->get();
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => 'required|string',
            'hours_cost' => 'required',
        ]);
        Technician::where('id', $id)->update([
            'name' => $request->input('name'),
            'hours_cost' => $request->input('hours_cost'),
        ]);
        $response = Technician::where('id', $id)->get();
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $technician = Technician::findOrFail($id);
        $technician->delete();
        return response()->json(["message" => "service deleted"], 200);
    }
}
