<?php

namespace App\Listeners;

use App\Events\TicketStatusUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SetStatusToPending
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketStatusUpdated $event): void
    {
        $ticket = $event->ticket;
        if ($ticket->client_id && $ticket->service_id && $ticket->priority_id) {
            $ticket->status = '1';
            $ticket->save();
    }
}
}