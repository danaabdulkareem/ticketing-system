<?php

namespace App\Http\Controllers\Ticket;

use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Events\TicketStatusUpdated;
use App\Http\Controllers\Controller;
use Spatie\QueryBuilder\QueryBuilder;
use App\Http\Requests\UpdateTicketRequest;
use App\Services\CalcTicketCost;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $ticket = Ticket::all();
        return $ticket;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'service_id' => 'required|exists:services,id',
            'creation_date' => 'required|date'
        ]);
        $ticket = Ticket::create([
            'client_id' => auth()->user()->id,
            'service_id' => $request['service_id'],
            'creation_date' => $request['creation_date']
        ]);
        $response = $ticket->get();
        event(new TicketStatusUpdated($ticket));
        return response()->json(['ticket' => $response], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTicketRequest $request, string $id)
    {
        $attributes = $request->validated();
        $ticket = Ticket::where('id', $id)->first();
        if (!$ticket) {
            return response(["message" => "no such ticket found"]);
        }
        $technicianIds = $request->input('technician_ids');
        $ticket->update($attributes);
        $ticket->technician()->attach($technicianIds);
        TicketStatusUpdated::dispatch($ticket);
        return response()->json(['message' => 'Ticket updated successfully']);
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $ticket = Ticket::findOrFail($id);
        $ticket->delete();
        return response()->json(['message' => 'Ticket deleted successfully']);
    }
    public function filter()
    {
        $tickets = QueryBuilder::for(Ticket::class)
            ->allowedFilters(['creation_date'])
            ->get();
        return response(['tickets', $tickets], 200);
    }
    public function clacCost($ticketId)
    {
        return (new CalcTicketCost())->updatetotalcost($ticketId);
    }
    
}
