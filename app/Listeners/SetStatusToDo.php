<?php

namespace App\Listeners;

use App\Events\TicketStatusUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SetStatusToDo
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketStatusUpdated $event): void
    {
        $ticket = $event->ticket;
        if ($ticket->work_report && $ticket->work_completion_date && $ticket->client_notes && $ticket->total_cost && $ticket->client_evaluation) {
            $ticket->status = '3';
            $ticket->save();
        }
}
}
