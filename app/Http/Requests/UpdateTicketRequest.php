<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'priority_id' => 'integer|exists:priorities,id',
            'status_id' => 'integer|exists:statuses,id',
            'service_id' => 'integer|exists:services,id',
            'creation_date' => 'date',
            'total_working_hours' => 'numeric',
            'technicians_count' => 'integer',
            'work_report' => 'string',
            'work_completion_date' => 'date',
            'client_notes' => 'string',
            'client_evaluation' => 'in:3,2,1',
            'total_cost' => 'nullable|numeric',
        ];
    }
}
