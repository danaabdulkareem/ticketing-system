<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    use HasFactory;
    protected $guarded=[];
    public function ticket()
    {
        return $this->belongsToMany(Technician::class,'technician_ticket','technician_id','ticket_id',);
    }
}
