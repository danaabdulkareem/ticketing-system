<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\ClientAuthController;
use App\Http\Controllers\Ticket\TicketController;

Route::post('/admin/login', [AdminAuthController::class, 'login']);
Route::post('/client/login', [ClientAuthController::class, 'login']);
Route::apiResource('services', 'App\Http\Controllers\ServiceController')->only(['index']);

Route::middleware('auth:sanctum')->group(function () {

    Route::middleware('type.admin')->group(function () {
        Route::post('/admin/logout', [AdminAuthController::class, 'logout']);
        Route::apiResource('clients', 'App\Http\Controllers\ClientController');
        Route::apiResource('statuses', 'App\Http\Controllers\StatusController');
        Route::apiResource('services', 'App\Http\Controllers\ServiceController')->except(['index']);
        Route::apiResource('technicians', 'App\Http\Controllers\TechnicianController');
        Route::apiResource('priorities', 'App\Http\Controllers\PriorityController');
        Route::get('/filter', [TicketController::class, 'filter']);
        Route::get('/totalcost/{id}', [TicketController::class, 'clacCost']);
    });

    Route::middleware('type.client')->group(function () {
        Route::post('/client/logout', [ClientAuthController::class, 'logout']);
        //.. another routes like  evaluate,add notes,..etc 
    });
    Route::apiResource('tickets', 'App\Http\Controllers\Ticket\TicketController');
});
