<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'priority_id',
        'status_id',
        'creation_date',
        'total_working_hours',
        'technician_id',
        'work_report',
        'work_completion_date',
        'client_notes',
        'client_evaluation',
        'total_cost',
        'service_id'
    ];

    // Define relationships
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    public function technician()
    {
        return $this->belongsToMany(Technician::class,'technician_ticket', 'ticket_id','technician_id');
    }
}
