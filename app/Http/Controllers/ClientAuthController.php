<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ClientAuthController extends Controller
{
    public function login(Request $request)
{
        $request->validate([
            'phone_number' => 'required',
            'password' => 'required',
        ]);
        $client = Client::where('phone_number', $request->phone_number)->first();
        if (!$client || !Hash::check($request->password, $client->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
        return response()->json([
            'client' => $client,
            'token' => $client->createToken('mobile', ['role:client'])->plainTextToken
        ]);
}
public function logout(Request $request)
{
    Auth::user()->tokens()->delete();
    return response('done');
}
}
