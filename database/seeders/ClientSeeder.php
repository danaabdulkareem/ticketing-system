<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       Client::create([
            'id' => '1',
            'name' => "Dana Ak",
            'phone_number' => '0994521351',
            'password'=>bcrypt('1234567*'),
        ]);
    }
}
