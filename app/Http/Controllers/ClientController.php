<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $clients=Client::all();
        return $clients;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
            $request->validate([
            'name'=>'required|string',
            'phone_number' => 'required|integer|max:10',
            'password' => 'required',
        ]);
        $client = Client::create([
            'name' => $request['name'],
            'password' => bcrypt($request['password']),
            'phone_number' => $request['phone_number'],
        ]);
        $response = $client->get();
            return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name'=>'required|string',
            'phone_number' => 'required|integer|max:10',]);
            Client::where('id',$id)->update([
                'name' => $request->input('name'),
            'phone_number' => $request->input('phone_number'),]);
            $response = Client::where('id',$id)->get();
            return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $client=Client::findOrFail($id);
        $client->delete();
        return response()->json(["message"=>"client deleted"],200);
    }
}
