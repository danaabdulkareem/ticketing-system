<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $priorities = Service::all();
        return $priorities;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'price' => 'required',
            'category' => 'required'
        ]);
        $service = Service::create([
            'title' => $request['title'],
            'price' => $request['price'],
            'category' => $request['category'],
        ]);
        $response = $service->get();
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required|string',
            'price' => 'required',
            'category' => 'required'
        ]);
        Service::where('id', $id)->update([
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'category' => $request->input('category'),
        ]);
        $response = Service::where('id', $id)->get();
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
        return response()->json(["message" => "service deleted"], 200);
    }
}
