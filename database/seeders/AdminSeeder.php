<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Admin::create([
            'id' => '1',
            'name' => "Dana Abdulkareem",
            'email' => 'abdulkareemdana2000@gmail.com',
            'password'=>bcrypt('1234567*'),
        ]);
    }
}
